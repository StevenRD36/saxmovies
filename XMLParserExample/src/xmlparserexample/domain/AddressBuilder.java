
package xmlparserexample.domain;

import xmlparserexample.domain.Address;

/**
 *
 * @author SDOAX36
 */
public class AddressBuilder {
    
    private Address address;
    
    public AddressBuilder(){
        address = new Address();
    }
    
    public AddressBuilder setStreet(String street){
        this.address.setStreet(street);
        return this;
    }
    
    public AddressBuilder setNumber(String number){
        this.address.setNumber(number);
        return this;
    }
    
    public AddressBuilder setCity(String city){
        this.address.setCity(city);
        return this;
    }
    
    public AddressBuilder setPostalCode(String postalCode){
        this.address.setPostalCode(postalCode);
        return this;
    }
    
    public Address build(){
        return this.address;
    }
}
