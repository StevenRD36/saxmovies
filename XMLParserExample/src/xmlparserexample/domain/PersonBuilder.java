/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlparserexample.domain;

import xmlparserexample.domain.Address;

/**
 *
 * @author SDOAX36
 */
public class PersonBuilder {
    
    
    private Person person;
    
    public PersonBuilder(){
        person = new Person();
    }
    
    public PersonBuilder setId(Long id){
        person.setId(id);
        return this;
    }
    
    public PersonBuilder setFirstName(String fn){
        person.setFirstName(fn);
        return this;
    }
    public PersonBuilder setLastName(String ln){
        person.setLastName(ln);
        return this;
    }
    
    public PersonBuilder setGender(String gender){
        person.setGender(gender);
        return this;
    }
    public PersonBuilder setRole(String role){
        person.setRole(role);
        return this;
    }
    public PersonBuilder setAddress(Address address){
        person.setAddress(address);
        return this;
    }
    
    public Person build(){
        return person;
    }
                    
                
}
