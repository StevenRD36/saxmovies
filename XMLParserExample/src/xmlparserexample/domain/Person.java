
package xmlparserexample.domain;

import xmlparserexample.domain.Address;
import java.time.LocalDate;


public class Person {
    
    private Long id;
    private String firstName, lastName;
    private LocalDate birthDate;
    private int age;
    private Address address;
    private String role;
    private String gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" + "id= " + id 
                + ", firstName= " + firstName 
                + ", lastName= " + lastName 
                + ", age= " + age 
                + ", role= " + role 
                + ", gender= " + gender 
                + "\n"+address.toString()+
                '}';
    }
    
    
    
}
