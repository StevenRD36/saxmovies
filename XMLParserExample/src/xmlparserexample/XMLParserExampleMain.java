package xmlparserexample;

import xmlparserexample.dom.DOMParserExample;
import xmlparserexample.domain.PersonBuilder;
import xmlparserexample.domain.AddressBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLInputFactory;
import jdk.internal.org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import xmlparserexample.stax.StaxEmployeeWriter;
import xmlparserexample.stax.StaxParserExample;


/**
 *
 * @author SDOAX36
 */
public class XMLParserExampleMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){

            //Parse with SAX
//        SAXParserExample sax = new SAXParserExample();
//        sax.printXmlDocument();


//Write with DOM
//        DOMParserExample dom = new DOMParserExample();
//        
//        dom.writePersonToXml(new PersonBuilder().setId(22L)
//                .setFirstName("Steven")
//                .setLastName("De Cock")
//                .setGender("Male")
//                .setRole("Dev")
//                .setAddress(new AddressBuilder()
//                        .setStreet("ThugStreet")
//                        .setNumber("12")
//                        .setCity("Compton")
//                        .setPostalCode("666")
//                .build())
//        .build());
//read with dom
//        dom.printPeople();

//        StaxParserExample stax = new StaxParserExample();
//        stax.readXml();

        StaxEmployeeWriter writer = new StaxEmployeeWriter();
        writer.writeToFile(new PersonBuilder().setId(22L)
                .setFirstName("Steven")
                .setLastName("De Cock")
                .setGender("Male")
                .setRole("Dev")
                .setAddress(new AddressBuilder()
                        .setStreet("ThugStreet")
                        .setNumber("12")
                        .setCity("Compton")
                        .setPostalCode("666")
                .build())
        .build());
    }
}
