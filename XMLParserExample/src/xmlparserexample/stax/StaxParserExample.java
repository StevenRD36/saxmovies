/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlparserexample.stax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import xmlparserexample.XMLParserExampleMain;
import xmlparserexample.domain.AddressBuilder;
import xmlparserexample.domain.Person;
import xmlparserexample.domain.PersonBuilder;

/**
 *
 * @author SDOAX36
 */
public class StaxParserExample {
    
    private List<Person> people;
    
    public void readXml(){
                FileInputStream fileInputStream = null;
        try {
        //create XmlInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            //create file inputstream from file
            fileInputStream = new FileInputStream("people.xml");
            //create XmlEventReader from inputfactory with fileInputStream
            XMLEventReader eventReader = inputFactory.createXMLEventReader(fileInputStream);
            
            PersonBuilder pb = null;
            AddressBuilder ab = null;
            people = new ArrayList<>();
            //start reading
            while(eventReader.hasNext()){
                XMLEvent event = eventReader.nextEvent();
                
                //check if we are in the start element
                if(event.isStartElement()){
                    StartElement start = event.asStartElement();
                    if(start.getName().getLocalPart().equals("Employee")){
                        pb = new PersonBuilder();
                        QName id = new QName("id");
                        Attribute attr = start.getAttributeByName(id);
                        if(attr!=null){
                            pb.setId(Long.parseLong(attr.getValue()));
                        }
                    }
                    else if(start.getName().getLocalPart().equals("firstname")){
                        event = eventReader.nextEvent();
                        pb.setFirstName(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equals("lastname")){
                        event = eventReader.nextEvent();
                        pb.setLastName(event.asCharacters().getData());
                        continue;
                    }
         
                    else if(start.getName().getLocalPart().equals("gender")){
                        event = eventReader.nextEvent();
                        pb.setGender(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equals("role")){
                        event = eventReader.nextEvent();
                        pb.setRole(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equals("address")){
                        System.out.println("Address start klodden");
                        ab = new AddressBuilder();
                    }
                    else if(start.getName().getLocalPart().equals("street")){
                        event = eventReader.nextEvent();
                        ab.setStreet(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equals("number")){
                        event = eventReader.nextEvent();
                        ab.setNumber(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equals("city")){
                        event = eventReader.nextEvent();
                        ab.setCity(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equals("postalcode")){
                        event = eventReader.nextEvent();
                        ab.setPostalCode(event.asCharacters().getData());
                        continue;
                    }


                    }
                
                if(event.isEndElement()){
                    System.out.println("is end");
                    EndElement endElement = event.asEndElement();
                if(endElement.getName().getLocalPart().equals("address")){
                    pb.setAddress(ab.build());
                   }
                if(endElement.getName().getLocalPart().equals("Employee") ) {
                    people.add(pb.build());
                    }      
                }
                       
            }
            
            people.stream().forEach(p->System.out.println(p));
            
        } catch (FileNotFoundException | XMLStreamException ex) {
            ex.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
