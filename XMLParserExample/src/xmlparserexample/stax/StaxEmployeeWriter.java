
package xmlparserexample.stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.XMLEvent;
import xmlparserexample.domain.Person;

/**
 *
 * @author SDOAX36
 */
public class StaxEmployeeWriter {
    
    
    public void writeToFile(Person person){

        try {
            Map<QName,String>peopleMap = new HashMap<>();
            peopleMap.put(new QName("firstname"), person.getFirstName());
            peopleMap.put(new QName("lastname"), person.getLastName());
            peopleMap.put(new QName("gender"), person.getGender());
            peopleMap.put(new QName("role"), person.getRole());
            //get the writer and eventfactory
            XMLEventFactory eventFactory = XMLEventFactory.newInstance();
            XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(new FileWriter(new File("people.xml")));
//            String file = getClass().getResource("people.xml").getPath();
//            XMLEventReader eventReader = inputFactory.createXMLEventReader(new FileInputStream("people.xml"));
            // Attributes
            Attribute att = eventFactory.createAttribute(new QName("id"), person.getId().toString());
            Attribute att2= eventFactory.createAttribute(new QName("name"),"Jasper");
            ArrayList attArr = new ArrayList();
            attArr.add(att);
            attArr.add(att2);
            // Namespaces
            ArrayList nameArr = new ArrayList();
             QName qname = new QName("Employee");
             QName addresQ = new QName("address");
//             StartDocument event = null;
//             while(eventReader.hasNext()){
//                 XMLEvent xml = eventReader.nextEvent();
//                
//                 if(xml.isStartDocument()){
//                     event = (StartDocument) xml;
//                 }
//             } 
            // Create and write elements
            eventWriter.add(eventFactory.createStartElement(qname, attArr.iterator(), nameArr.iterator()));
            attArr = new ArrayList();
            for(Map.Entry<QName,String> entry : peopleMap.entrySet()){
                eventWriter.add(eventFactory.createStartElement(entry.getKey(), attArr.iterator(), nameArr.iterator()));
                eventWriter.add(eventFactory.createCharacters(entry.getValue()));
                eventWriter.add(eventFactory.createEndElement(qname,attArr.iterator()));
            }
            
            eventWriter.add(eventFactory.createStartElement(addresQ, attArr.iterator(),nameArr.iterator()));
            
            Map<QName,String> address = new HashMap<>();
            address.put( new QName("street"), person.getAddress().getStreet());
            address.put( new QName("number"), person.getAddress().getNumber());
            address.put( new QName("city"), person.getAddress().getCity());
            address.put( new QName("postalcode"), person.getAddress().getPostalCode());
            
            for(Map.Entry<QName,String> entry : address.entrySet()){
                eventWriter.add(eventFactory.createStartElement(entry.getKey(), attArr.iterator(), nameArr.iterator()));
                eventWriter.add(eventFactory.createCharacters(entry.getValue()));
                eventWriter.add(eventFactory.createEndElement(entry.getKey(), nameArr.iterator()));
            }
            
            eventWriter.add(eventFactory.createEndElement(addresQ,nameArr.iterator()));
            eventWriter.add(eventFactory.createEndElement(qname,attArr.iterator()));

            eventWriter.flush();  // When underlying stream is buffered
            eventWriter.close();
            
        } catch (XMLStreamException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StaxEmployeeWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StaxEmployeeWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
