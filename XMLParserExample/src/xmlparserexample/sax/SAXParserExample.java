
package xmlparserexample.sax;

import xmlparserexample.sax.MyHandler;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import xmlparserexample.XMLParserExampleMain;

/**
 *
 * @author SDOAX36
 */
public class SAXParserExample {
    
    public void printXmlDocument(){
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

            SAXParser saxParser = saxParserFactory.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            MyHandler handler = new MyHandler();
            xmlReader.setContentHandler(handler);
            xmlReader.parse(new InputSource("people.xml"));
            handler.getPersons().stream().forEach(p->System.out.println(p.toString()));
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            Logger.getLogger(XMLParserExampleMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
