
package xmlparserexample.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author SDOAX36
 */
public class PrintHandler extends DefaultHandler{

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        System.out.println("Characters : "+ch+" "+start+" "+length+" "+new String(ch,start,length).toString()); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        System.out.println("End element : "+qName);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        System.out.println("Start element "+qName);
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("End document");
    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Start Document");
    }
    
    
}
