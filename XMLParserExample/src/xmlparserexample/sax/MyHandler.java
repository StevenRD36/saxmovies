package xmlparserexample.sax;

import xmlparserexample.domain.Person;
import xmlparserexample.domain.Address;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author SDOAX36
 */
public class MyHandler extends DefaultHandler{

       
    private List<Person> persons;
    private Person p;
    private Address a;
    
    private boolean pFirstName;
    private boolean pLastName;
    private boolean pAge;
    private boolean pGender;
    private boolean pRole;
    
    private boolean aStreet;
    private boolean aNumber;
    private boolean aCity;
    private boolean aPostalCode;
    

    @Override
    public void startDocument() {
        persons = new ArrayList<>();
    }


    @Override
    public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
        if("employee".equalsIgnoreCase(qName)){
            p = new Person();
            
            p.setId(Long.valueOf(attributes.getValue("id")));
        }
        
        if("address".equalsIgnoreCase(qName)){
            a = new Address();
            
        }
        
        switch(qName.toLowerCase()){
            case "firstname": pFirstName = true;
            break;
            case "lastname": pLastName = true;
            break;
            case "age": pAge = true;
            break;
            case "gender": pGender = true;
            break;
            case "role": pRole = true;
            break;
            case "street": aStreet = true;
            break;
            case "number": aNumber = true;
            break;
            case "city": aCity = true;
            break;
            case "postalcode": aPostalCode = true;
            break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws org.xml.sax.SAXException {
        if(pFirstName){
            p.setFirstName(new String(ch,start,length));
            pFirstName = false;
        }
        if(pLastName){
            p.setLastName(new String(ch,start,length));
            pLastName = false;
        }
        if(pAge){
            p.setAge(Integer.parseInt(new String(ch,start,length)));
            pAge = false;
        }
        if(pGender){
            p.setGender(new String(ch,start,length));
            pGender = false;
        }
        if(pRole){
            p.setRole(new String(ch,start,length));
            pRole = false;
        }
        if(aStreet){
            a.setStreet(new String(ch,start,length));
            aStreet = false;
        }
        if(aNumber){
            a.setNumber(new String(ch,start,length));
            aNumber = false;
        }
        if(aCity){
            a.setCity(new String(ch,start,length));
            aCity = false;
        }
        if(aPostalCode){
            a.setPostalCode(new String(ch,start,length));
            aPostalCode = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws org.xml.sax.SAXException {
       
        if("employee".equalsIgnoreCase(qName) && p != null){
            persons.add(p);
        }
        if("address".equalsIgnoreCase(qName) && a!=null){
            p.setAddress(a);
        }
        
    }

    public List<Person> getPersons() {
        return persons;
    }
   
}
