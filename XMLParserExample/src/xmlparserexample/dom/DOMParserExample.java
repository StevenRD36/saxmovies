
package xmlparserexample.dom;

import xmlparserexample.domain.Person;
import xmlparserexample.domain.Address;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author SDOAX36
 */
public class DOMParserExample {
    
    private List<Person> people;
    
    public DOMParserExample(){
        people = new ArrayList<>();
    }
    
    public void printPeople(){
        readXmlDocument();
        people.stream().forEach(p->System.out.println(p.toString()));
    }
    
    private void readXmlDocument(){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("people.xml");
            NodeList nodeList = document.getElementsByTagName("Employee");
            for(int i = 0; i< nodeList.getLength(); i++){
                Node node = nodeList.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE){
                    Person p = new Person();
                    //element node for employee
                    Element employeeElement = (Element)node; 
                    //retrieve the id
                    Long id = Long.parseLong(employeeElement.getAttribute("id"));
                    p.setId(id);
                    String firstName = employeeElement.getElementsByTagName("firstname").item(0).getTextContent();
                    String lastName = employeeElement.getElementsByTagName("lastname").item(0).getTextContent();
                    String gender = employeeElement.getElementsByTagName("gender").item(0).getTextContent();
                    String role = employeeElement.getElementsByTagName("role").item(0).getTextContent();
                    
                    Node addressNode = employeeElement.getElementsByTagName("address").item(0);
                    Element addressElement = (Element) addressNode;
                    String street = addressElement.getElementsByTagName("street").item(0).getTextContent();
                    String number = addressElement.getElementsByTagName("number").item(0).getTextContent();
                    String city = addressElement.getElementsByTagName("city").item(0).getTextContent();
                    String postalCode = addressElement.getElementsByTagName("postalcode").item(0).getTextContent();
                    
                    p.setFirstName(firstName);
                    p.setLastName(lastName);
                    p.setGender(gender);
                    p.setRole(role);
                    Address address = new Address();
                    address.setStreet(street);
                    address.setNumber(number);
                    address.setCity(city);
                    address.setPostalCode(postalCode);
                    p.setAddress(address);
                    
                    people.add(p);
                }
                
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DOMParserExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void writePersonToXml(Person person){
        try{    
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("people.xml");
            
            Element employeeElement = document.createElement("Employee");
            employeeElement.setAttribute("id", person.getId().toString());
            List<Element> elements = new ArrayList<>();
            Element fnElement = document.createElement("firstname");
            fnElement.setTextContent(person.getFirstName());
            elements.add(fnElement);
            
            Element lnElement = document.createElement("lastname");
            lnElement.setTextContent(person.getLastName());
            elements.add(lnElement);

            Element genderElement = document.createElement("gender");
            genderElement.setTextContent(person.getGender());
            elements.add(genderElement);
            
            Element roleElement = document.createElement("role");
            roleElement.setTextContent(person.getRole());
            elements.add(roleElement);
            
            Element addressElement = document.createElement("address");
            Element streetElement = document.createElement("street");
            streetElement.setTextContent(person.getAddress().getStreet());
            
            Element numberElement = document.createElement("number");
            numberElement.setTextContent(person.getAddress().getNumber());
            
            Element cityElement = document.createElement("city");
            cityElement.setTextContent(person.getAddress().getCity());
            
            Element postalElement = document.createElement("postalcode");
            postalElement.setTextContent(person.getAddress().getPostalCode());
            
            addressElement.appendChild(streetElement);
            addressElement.appendChild(numberElement);
            addressElement.appendChild(cityElement);
            addressElement.appendChild(postalElement);
            
            elements.add(addressElement);
            
            for(Element e : elements){
                employeeElement.appendChild(e);
            }
            
            document.getDocumentElement().appendChild(employeeElement);
            writeXmlToFile(document, "result.xml");
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DOMParserExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void writeXmlToFile(Document doc,String file){
        try {
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("result.xml"));
            transformer.transform(source, result);
            
            // Output to console for testing
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(DOMParserExample.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(DOMParserExample.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
