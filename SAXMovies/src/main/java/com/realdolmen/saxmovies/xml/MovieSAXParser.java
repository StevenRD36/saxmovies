/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.xml;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author SDOAX36
 */
public class MovieSAXParser {
    
    
    public void parseXmlFile(){
        try {
             //TODO create SAXParserFactory and SAXParser
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            //TODO get the XMLReader from the SAXParser
            XMLReader xmlReader = saxParser.getXMLReader();
            //TODO create new MovieHandler
            MovieHandler handler = new MovieHandler();
            
            xmlReader.setContentHandler(handler);
            //TODO parse the file movies.xml
            xmlReader.parse(new InputSource("movies.xml"));
            handler.getMovies().stream().forEach(p->System.out.println(p.toString()));
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
