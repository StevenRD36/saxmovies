/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.xml;

import com.realdolmen.saxmovies.domain.Actor;
import com.realdolmen.saxmovies.domain.Director;
import com.realdolmen.saxmovies.domain.Movie;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author SDOAX36
 */
public class MovieHandler extends DefaultHandler{

    private List<Movie> movies;
    private List<Actor> actors;
    private Movie movie;
    private Actor actor;
    private Director director;
    
    private boolean mTitle,mYear,mCountry,mGenre,mSummary,mFirstName,mLastName, mBirthDate,mDirector,mActors,mRole;
         
    
    
    @Override
    public void startDocument() throws SAXException {
        movies = new ArrayList<>();
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if("movie".equalsIgnoreCase(qName)){
            movie = new Movie();
        }
        if("director".equalsIgnoreCase(qName)){
            director = new Director();
            mDirector = true;
        }
        if("actors".equalsIgnoreCase(qName)){
            actors = new ArrayList<>();
            mActors = true;
        }
        if("actor".equalsIgnoreCase(qName)){
            actor = new Actor();
        }

        switch(qName.toLowerCase()){
            case "title" : mTitle = true;
            break;
            case "genre" : mGenre = true;
            break;
            case "year" : mYear = true;
            break;
            case "country" : mCountry = true;
            break;
            case "first_name" : mFirstName = true;
            break;
            case "last_name" : mLastName = true;
            break;
            case "birth_date" : mBirthDate = true;
            break;
            case "role" : mRole = true;
            break;      
        }
    
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(mTitle){
            movie.setTitle(new String(ch,start,length));
            mTitle = false;
        }
        if(mGenre){
            movie.setGenre(new String(ch,start,length));
            mGenre = false;
        }
        if(mCountry){
            movie.setCountry(new String(ch,start,length));
            mCountry = false;
        }
        if(mYear){
            movie.setYear(new String(ch,start,length));
            mYear = false;
        }
        if(mActors){
            if(mFirstName){
                actor.setFirstName(new String(ch,start,length));
                mFirstName = false;
            }
            if(mLastName){
                actor.setLastName(new String(ch,start,length));
                mLastName = false;

            }
            if(mBirthDate){
                  actor.setBirthDate(new String(ch,start,length));
                  mBirthDate = false;
            }
            if(mRole){
                actor.setRole(new String(ch,start,length));
                mRole = false;
            }
        }
        if(mDirector){
            if(mFirstName){
                director.setFirstName(new String(ch,start,length));
                mFirstName = false;
            }
            if(mLastName){
                director.setLastName(new String(ch,start,length));
                mLastName = false;
            }
            if(mBirthDate){
                director.setBirthDate(new String(ch,start,length));
                mBirthDate = false;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if("movie".equalsIgnoreCase(qName)){
            movies.add(movie);
        }
        if("director".equalsIgnoreCase(qName)){
            movie.setDirector(director);
            mDirector = false;
        }
        if("actors".equalsIgnoreCase(qName)){
            movie.setActors(actors);
            mActors = false;
        }
        if("actor".equalsIgnoreCase(qName)){
            actors.add(actor);
        }
       
    }

    public List<Movie> getMovies() {
        return movies;
    }    
    
}
