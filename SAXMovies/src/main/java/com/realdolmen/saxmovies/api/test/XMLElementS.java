
package com.realdolmen.saxmovies.api.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author SDOAX36
 */

@Retention(RetentionPolicy.RUNTIME)

public @interface XMLElementS {
    
}
