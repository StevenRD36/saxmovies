
package com.realdolmen.saxmovies;

import com.realdolmen.saxmovies.domain.ActorBuilder;
import com.realdolmen.saxmovies.domain.Director;
import com.realdolmen.saxmovies.domain.DirectorBuilder;
import com.realdolmen.saxmovies.domain.MovieBuilder;
import com.realdolmen.saxmovies.domparsers.ActorDOMReadWriter;
import com.realdolmen.saxmovies.domparsers.DOMMovieParser;
import com.realdolmen.saxmovies.domparsers.DOMMovieWriter;
import com.realdolmen.saxmovies.stax.StaxMovieParser;
import com.realdolmen.saxmovies.stax.StaxMovieWriter;
import com.realdolmen.saxmovies.xml.MovieHandler;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;


/**
 *
 * @author SDOAX36
 */
public class SAXMoviesMain {
    
    
    public static void main(String[] args) {
  
        DOMMovieWriter dom = new DOMMovieWriter();
//        dom.writeMovieToXml(new MovieBuilder()
//                .setTitle("Test")
//                .setCountry("Test")
//                .setYear("Test")
//                .setGenre(".sdmlfj")
//                .setDirector(new DirectorBuilder()
//                    .setFirstName("Steven")
//                    .setLastName("Spielberg")
//                    .setBirthDate("ooit")
//                        .build())
//                .setActors(Arrays.asList(new ActorBuilder()
//                    .setFirstName("actor")
//                    .setLastName("van mijn kloten")
//                    .setBirthDate("gisteren")
//                    .setRole("nen dooien")
//                    .build()))
//                .build());

//        ActorDOMReadWriter actorDOMReadWriter = new ActorDOMReadWriter();
//        actorDOMReadWriter.readActors();

//        StaxMovieParser movieParser = new StaxMovieParser();
//        movieParser.readXmlFile();

        StaxMovieWriter writer = new StaxMovieWriter();
        writer.writeMovieToXml(new MovieBuilder()
                .setTitle("Test")
                .setCountry("Test")
                .setYear("Test")
                .setGenre(".sdmlfj")
                .setDirector(new DirectorBuilder()
                    .setFirstName("Steven")
                    .setLastName("Spielberg")
                    .setBirthDate("ooit")
                        .build())
                .setActors(Arrays.asList(new ActorBuilder()
                    .setFirstName("actor")
                    .setLastName("van mijn kloten")
                    .setBirthDate("gisteren")
                    .setRole("nen dooien")
                    .build()))
                .build());
    }
}
