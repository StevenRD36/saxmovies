/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.domparsers;

import com.realdolmen.saxmovies.domain.Actor;
import com.realdolmen.saxmovies.domain.ActorBuilder;
import com.realdolmen.saxmovies.domain.DirectorBuilder;
import com.realdolmen.saxmovies.domain.Movie;
import com.realdolmen.saxmovies.domain.MovieBuilder;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import org.xml.sax.SAXException;


/**
 *
 * @author SDOAX36
 */
public class DOMMovieParser {
    
    private List<Movie> movies;

    public DOMMovieParser() {
        movies = new ArrayList<>();
    }
    
    public void parseXmlFile(){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("movies.xml"));
            
            NodeList nodeList = doc.getElementsByTagName("movie");
            for(int i = 0; i<nodeList.getLength();i++){
                if(nodeList.item(i).getNodeType() == Node.ELEMENT_NODE){
                    
                    Element movieEl = (Element) nodeList.item(i);
                    String title = movieEl.getElementsByTagName("title").item(0).getTextContent();
                    String year = movieEl.getElementsByTagName("year").item(0).getTextContent();
                    String country = movieEl.getElementsByTagName("country").item(0).getTextContent();
                    String genre = movieEl.getElementsByTagName("genre").item(0).getTextContent();
                    String summary = "";
                    NodeList sumList = movieEl.getElementsByTagName("summary");
                    if(sumList.getLength()>0){
                       summary = sumList.item(0).getTextContent();
                    }
                    Element directorEl = (Element)movieEl.getElementsByTagName("director").item(0);
                    String dFirstName = directorEl.getElementsByTagName("first_name").item(0).getTextContent();
                    String dLastName = directorEl.getElementsByTagName("last_name").item(0).getTextContent();
                    String dBirthDate = directorEl.getElementsByTagName("birth_date").item(0).getTextContent();
                    
                    NodeList actorsEl = movieEl.getElementsByTagName("actor");
                    List<Actor> actors = new ArrayList<>();
                    for(int j = 0; j< actorsEl.getLength();j++){
                        Element actorEl = (Element)actorsEl.item(j);
                        String aFirstName = actorEl.getElementsByTagName("first_name").item(0).getTextContent();
                        String aLastName = actorEl.getElementsByTagName("last_name").item(0).getTextContent();
                        String aBirthName = actorEl.getElementsByTagName("birth_date").item(0).getTextContent();
                        String aRole = actorEl.getElementsByTagName("role").item(0).getTextContent();
                        actors.add(new ActorBuilder()
                                .setFirstName(aFirstName)
                                .setLastName(aLastName)
                                .setBirthDate(aBirthName)
                                .setRole(aRole).build());
                                
                    }
                    
                    movies.add(new MovieBuilder()
                            .setTitle(title)
                            .setCountry(country)
                            .setYear(year)
                            .setSummary(summary)
                            .setGenre(genre)
                            .setDirector(new DirectorBuilder()
                                    .setFirstName(dFirstName)
                                    .setLastName(dLastName)
                                    .setBirthDate(dBirthDate).build())
                            .setActors(actors)
                            .build());
                    
                }
            }
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DOMMovieParser.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public void printMovies(){
        if(movies.isEmpty()){
            parseXmlFile();
        }
        movies.stream().forEach(m->System.out.println(m.toString()));
    }
    
}
