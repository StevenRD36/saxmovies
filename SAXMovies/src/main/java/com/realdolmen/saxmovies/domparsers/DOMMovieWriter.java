/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.domparsers;

import com.realdolmen.saxmovies.domain.Actor;
import com.realdolmen.saxmovies.domain.Movie;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author SDOAX36
 */
public class DOMMovieWriter {
    
    public void writeMovieToXml(Movie movie){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("movies.xml"));
           
                       
            List<Element> elements = new ArrayList<>();
            Element movieElement = doc.createElement("movie");
            
            Element titleEl = doc.createElement("title");
            titleEl.setTextContent(movie.getTitle());
            elements.add(titleEl);
                    
            Element countryEl = doc.createElement("country");
            countryEl.setTextContent(movie.getCountry());
            elements.add(countryEl);
            
            Element yearEl = doc.createElement("year");
            yearEl.setTextContent(movie.getYear());
            elements.add(yearEl);
            
            Element genreEl = doc.createElement("genre");
            genreEl.setTextContent(movie.getGenre());
            elements.add(yearEl);
            
            
            Element directorEl = doc.createElement("director");
            if(movie.getDirector()!=null){
                Element dFirstNameEl = doc.createElement("first_name");
                dFirstNameEl.setTextContent(movie.getDirector().getFirstName());
                directorEl.appendChild(dFirstNameEl);
                
                Element dLastNameEl = doc.createElement("last_name");
                dLastNameEl.setTextContent(movie.getDirector().getLastName());
                directorEl.appendChild(dLastNameEl);
                
                Element dBirthDateEl = doc.createElement("birth_date");
                dBirthDateEl.setTextContent(movie.getDirector().getBirthDate());
                directorEl.appendChild(dBirthDateEl);
            }
            
            elements.add(directorEl);
            Element actorsEl = doc.createElement("actors");
            for(Actor a : movie.getActors()){
                Element actorEl = doc.createElement("actor");
            
                Element aFirstNameEl = doc.createElement("first_name");
                aFirstNameEl.setTextContent(a.getFirstName());
                actorEl.appendChild(aFirstNameEl);
                
                Element aLastNameEl = doc.createElement("last_name");
                aLastNameEl.setTextContent(a.getLastName());
                actorEl.appendChild(aLastNameEl);
                
                Element aBirthDateEl = doc.createElement("birth_date");
                aBirthDateEl.setTextContent(a.getBirthDate());
                actorEl.appendChild(aBirthDateEl);
                
                Element aRoleEl = doc.createElement("role");
                aRoleEl.setTextContent(a.getRole());
                actorEl.appendChild(aRoleEl);
                
                actorsEl.appendChild(actorEl);
                
            }
            
            elements.add(actorsEl);
            
           
            for(Element el : elements){
                movieElement.appendChild(el);
            }
            doc.getDocumentElement().appendChild(movieElement);
            
            writeXmlToFile(doc, "result.xml");
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DOMMovieParser.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
     public static void writeXmlToFile(Document doc,String file){
        try {
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(file));
            transformer.transform(source, result);
            
            // Output to console for testing
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (TransformerConfigurationException ex) {
            ex.printStackTrace();
        } catch (TransformerException ex) {
            ex.printStackTrace();
        } 
    }
}
