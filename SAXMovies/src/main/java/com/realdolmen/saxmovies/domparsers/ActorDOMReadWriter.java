
package com.realdolmen.saxmovies.domparsers;

import com.realdolmen.saxmovies.domain.Actor;
import com.realdolmen.saxmovies.domain.ActorBuilder;
import com.realdolmen.saxmovies.domain.DirectorBuilder;
import com.realdolmen.saxmovies.domain.MovieBuilder;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author SDOAX36
 */
public class ActorDOMReadWriter {
    
    public void readActors(){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("movies.xml"));
            
            NodeList nodeList = doc.getElementsByTagName("actor"); 
            
            Document actorDoc = builder.newDocument();
            Element actorsElement = actorDoc.createElement("actors");
            actorDoc.appendChild(actorsElement);
            
            for(int i = 0 ;i < nodeList.getLength();i++){
                Node n = nodeList.item(i).cloneNode(true);
                actorDoc.adoptNode(n);
                actorDoc.getDocumentElement().appendChild(n);
            }
            
            DOMMovieWriter.writeXmlToFile(actorDoc, "actors.xml");
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DOMMovieParser.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
   
    
}
    

