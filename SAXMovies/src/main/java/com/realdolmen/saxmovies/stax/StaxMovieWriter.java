
package com.realdolmen.saxmovies.stax;

import com.realdolmen.saxmovies.domain.Actor;
import com.realdolmen.saxmovies.domain.Movie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;

/**
 *
 * @author SDOAX36
 */
public class StaxMovieWriter {
    
    public void writeMovieToXml(Movie movie){
        try {
            XMLEventFactory eventFactory = XMLEventFactory.newInstance();
            XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
            XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(System.out);
            // Attributes
            
            ArrayList attArr = new ArrayList();
            // Namespaces
//            Namespace namespace = eventFactory.createNamespace("foo","http://www.foo.org");
            ArrayList nameArr = new ArrayList();
//            nameArr.add(namespace);
            // Qualified names (element + namespace)
            QName qname = new QName("movie");
            QName qnameActors = new QName("actors");
            QName qnameDirector = new QName("director");
            QName qnameActor = new QName("actor");
            
            Map<QName,String> movieMap = new HashMap<>();
            Map<QName,String> directorMap = new HashMap<>();
            Map<QName,String>actorMap = null;
            
            movieMap.put(new QName("title"), movie.getTitle());
            movieMap.put(new QName("country"), movie.getCountry());
            movieMap.put(new QName("year"), movie.getYear());
            movieMap.put(new QName("genre"), movie.getGenre());
            
            directorMap.put(new QName("first_name"), movie.getDirector().getFirstName());
            directorMap.put(new QName("last_name"), movie.getDirector().getLastName());
            directorMap.put(new QName("birth_date"), movie.getDirector().getBirthDate());

            
            // Create and write movie elements
            eventWriter.add(eventFactory.createStartElement(qname, attArr.iterator(), nameArr.iterator()));
            for(Map.Entry<QName,String> entry : movieMap.entrySet()){
                eventWriter.add(eventFactory.createStartElement(entry.getKey(), attArr.iterator(), nameArr.iterator()));
                eventWriter.add(eventFactory.createCharacters(entry.getValue()));
                eventWriter.add(eventFactory.createEndElement(entry.getKey(),attArr.iterator()));
            }
            //create director
            eventWriter.add(eventFactory.createStartElement(qnameDirector, attArr.iterator(), nameArr.iterator()));
            for(Map.Entry<QName,String> entry : directorMap.entrySet()){
                eventWriter.add(eventFactory.createStartElement(entry.getKey(), attArr.iterator(), nameArr.iterator()));
                eventWriter.add(eventFactory.createCharacters(entry.getValue()));
                eventWriter.add(eventFactory.createEndElement(entry.getKey(),attArr.iterator()));
            }
            
            eventWriter.add(eventFactory.createEndElement(qnameDirector,attArr.iterator()));
            //create actors
            eventWriter.add(eventFactory.createStartElement(qnameActors, attArr.iterator(), nameArr.iterator()));
            for(Actor a : movie.getActors()){
                eventWriter.add(eventFactory.createStartElement(qnameActors, attArr.iterator(), nameArr.iterator()));
                actorMap = new HashMap<>();
                actorMap.put(new QName("first_name"), a.getFirstName());
                actorMap.put(new QName("last_name"), a.getLastName());
                actorMap.put(new QName("birth_date"), a.getBirthDate());
                actorMap.put(new QName("role"), a.getRole());
                for(Map.Entry<QName,String>entry : actorMap.entrySet()){
                    eventWriter.add(eventFactory.createStartElement(entry.getKey(), attArr.iterator(), nameArr.iterator()));
                    eventWriter.add(eventFactory.createCharacters(entry.getValue()));
                    eventWriter.add(eventFactory.createEndElement(entry.getKey(),attArr.iterator()));
                }
                eventWriter.add(eventFactory.createEndElement(qnameActor,attArr.iterator()));
            }
            eventWriter.add(eventFactory.createEndElement(qnameActors,attArr.iterator()));


            eventWriter.add(eventFactory.createEndElement(qname,attArr.iterator()));
            eventWriter.add(eventFactory.createEndDocument());
            eventWriter.flush();  // When underlying stream is buffered
            eventWriter.close();
            
        } catch (XMLStreamException ex) {
            Logger.getLogger(StaxMovieWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
