/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.stax;

import com.realdolmen.saxmovies.domain.Actor;
import com.realdolmen.saxmovies.domain.ActorBuilder;
import com.realdolmen.saxmovies.domain.DirectorBuilder;
import com.realdolmen.saxmovies.domain.Movie;
import com.realdolmen.saxmovies.domain.MovieBuilder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author SDOAX36
 */
public class StaxMovieParser {
    
    private List<Movie> movies;
    
    public StaxMovieParser(){
        movies = new ArrayList<>();
    }
    
    public void readXmlFile(){
        FileInputStream fileInputStream = null;
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            fileInputStream = new FileInputStream("movies.xml");
            
            XMLEventReader eventReader = factory.createXMLEventReader(fileInputStream);
            MovieBuilder mb = null;
            List<Actor> actors = null;
            DirectorBuilder db = null;
            ActorBuilder ab = null;
            
            boolean isDirector = false;
            boolean isActor = false;
            
            while(eventReader.hasNext()){
                XMLEvent event = eventReader.nextEvent();
                
                //start tags
                if(event.isStartElement()){
                    StartElement start = event.asStartElement();
                    if(start.getName().getLocalPart().equalsIgnoreCase("movie")){
                        mb = new MovieBuilder();
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("title")){
                        event = eventReader.nextEvent();
                        mb.setTitle(event.asCharacters().getData());
                        continue;
                    }
                     else if(start.getName().getLocalPart().equalsIgnoreCase("genre")){
                        event = eventReader.nextEvent();
                        mb.setGenre(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("year")){
                        event = eventReader.nextEvent();
                        mb.setYear(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("summary")){
                        event = eventReader.nextEvent();
                        mb.setSummary(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("country")){
                        event = eventReader.nextEvent();
                        mb.setCountry(event.asCharacters().getData());
                        continue;
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("director")){
                        db = new DirectorBuilder();
                        isDirector = true;
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("actors")){
                        actors = new ArrayList<>();
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("actor")){
                        ab = new ActorBuilder();
                        isActor = true;
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("first_name")){
                        if(isDirector){
                            event = eventReader.nextEvent();
                            db.setFirstName(event.asCharacters().getData());
                            continue;
                        }
                        if(isActor){
                            event = eventReader.nextEvent();
                            ab.setFirstName(event.asCharacters().getData());
                            continue;
                        }
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("last_name")){
                        if(isDirector){
                            event = eventReader.nextEvent();
                            db.setLastName(event.asCharacters().getData());
                            continue;
                        }
                        if(isActor){
                            event = eventReader.nextEvent();
                            ab.setLastName(event.asCharacters().getData());
                            continue;
                        }
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("birth_date")){
                        if(isDirector){
                            event = eventReader.nextEvent();
                            db.setBirthDate(event.asCharacters().getData());
                            continue;
                        }
                        if(isActor){
                            event = eventReader.nextEvent();
                            ab.setBirthDate(event.asCharacters().getData());
                            continue;
                        }
                    }
                    else if(start.getName().getLocalPart().equalsIgnoreCase("role")){
                        event = eventReader.nextEvent();
                        ab.setRole(event.asCharacters().getData());
                    }      
                }
                //end tags
                if(event.isEndElement()){
                    EndElement end = event.asEndElement();
                    if(end.getName().getLocalPart().equalsIgnoreCase("movie")){
                        movies.add(mb.build());
                    }
                    else if(end.getName().getLocalPart().equalsIgnoreCase("director")){
                        mb.setDirector(db.build());
                        isDirector = false;
                    }
                    else if(end.getName().getLocalPart().equalsIgnoreCase("actors")){
                        mb.setActors(actors);
                        actors = null;
                    }
                    else if(end.getName().getLocalPart().equalsIgnoreCase("actor")){
                        actors.add(ab.build());
                        isActor = false;
                    }
                }
            }
            movies.stream().forEach(m->System.out.println(m));
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StaxMovieParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMLStreamException ex) {
            Logger.getLogger(StaxMovieParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(StaxMovieParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
    
}
