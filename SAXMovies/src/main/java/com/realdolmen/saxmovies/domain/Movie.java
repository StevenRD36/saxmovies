
package com.realdolmen.saxmovies.domain;

import java.util.List;

/**
 *
 * @author SDOAX36
 */
public class Movie {
    
    //TODO add private attributes, getters and setters
    private String title,year,country,genre,summary;
    private Director director;
    private List<Actor>actors;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    @Override
    public String toString() {
        return "Movie{" + "title=" + title + ", year=" + year + ", country=" + country + ", genre=" + genre + ", summary=" + summary + ","
                + "\n\t director=" + director + ",\n\t\t actors=" + actors + '}';
    }
    
    
}
