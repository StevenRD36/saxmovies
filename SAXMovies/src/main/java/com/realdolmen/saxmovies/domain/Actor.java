/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.domain;

/**
 *
 * @author SDOAX36
 */
public class Actor extends Person{
    
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "{" + "role=" + role + super.toString()+"}\n\t\t";
    }
    
    
}
