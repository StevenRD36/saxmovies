/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.domain;

import java.util.List;

/**
 *
 * @author SDOAX36
 */
public class MovieBuilder {
    
    private Movie movie;


    public MovieBuilder() {
        movie = new Movie();
    }

    public MovieBuilder setTitle(String title) {
        this.movie.setTitle(title);
        return this;
    }

    public MovieBuilder setYear(String year) {
        this.movie.setYear(year);
        return this;
    }

    public MovieBuilder setCountry(String country) {
        this.movie.setCountry(country);
        return this;
    }

    public MovieBuilder setGenre(String genre) {
        this.movie.setGenre(genre);
        return this;
    }

    public MovieBuilder setSummary(String summary) {
        this.movie.setSummary(summary);
        return this;
    }

    public MovieBuilder setDirector(Director director) {
        this.movie.setDirector(director);
        return this;
    }

    public MovieBuilder setActors(List<Actor> actors) {
        this.movie.setActors(actors);
        return this;
    }
    
    public Movie build(){
        return this.movie;
    }
    
}
