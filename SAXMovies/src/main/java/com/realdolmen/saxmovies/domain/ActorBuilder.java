/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.domain;

/**
 *
 * @author SDOAX36
 */
public class ActorBuilder {
    
    private Actor actor;

    public ActorBuilder() {
        actor = new Actor();
    }
    
    public ActorBuilder setFirstName(String firstName){
        this.actor.setFirstName(firstName);
        return this;
    }
    
    public ActorBuilder setLastName(String lastName){
        this.actor.setLastName(lastName);
        return this;
    }
    
    public ActorBuilder setRole(String role){
        this.actor.setRole(role);
        return this;
    }
    
    public ActorBuilder setBirthDate(String birtString){
        this.actor.setBirthDate(birtString);
        return this;
    }
    
    public Actor build(){
        return this.actor;
    }
    
}
