
package com.realdolmen.saxmovies.domain;

/**
 *
 * @author SDOAX36
 */
public abstract class Person {
    
    private String firstName, lastName, birthDate;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "{" + "firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate + '}';
    }

    
    
}
