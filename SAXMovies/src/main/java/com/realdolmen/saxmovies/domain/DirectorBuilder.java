/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.saxmovies.domain;

/**
 *
 * @author SDOAX36
 */
public class DirectorBuilder {
           
       private Director director;

    public DirectorBuilder() {
        director = new Director();
    }

    public DirectorBuilder setFirstName(String firstName) {
        this.director.setFirstName(firstName);
        return this;
    }

    public DirectorBuilder setLastName(String lastName) {
        this.director.setLastName(lastName);
        return this;
    }

    public DirectorBuilder setBirthDate(String birthDate) {
        this.director.setBirthDate(birthDate);
        return this;
    }
    
    public Director build(){
        return this.director;
    }
    
       
       
}
